package xyz.hyron.ytchat;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleRefreshTokenRequest;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.LiveBroadcastListResponse;
import com.google.api.services.youtube.model.LiveChatMessage;
import com.google.api.services.youtube.model.LiveChatMessageListResponse;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.command.v2.ClientCommandManager;
import net.fabricmc.fabric.api.client.command.v2.ClientCommandRegistrationCallback;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientLifecycleEvents;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents.ServerStopping;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.hud.InGameHud;
import net.minecraft.command.CommandRegistryAccess;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.ClickEvent;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import static xyz.hyron.ytchat.ClientSecrets.*;

/**
 * Sample Java code for youtube.liveBroadcasts.list
 * See instructions for running these code samples locally:
 * https://developers.google.com/explorer-help/code-samples#java
 */

public class YTChat implements ModInitializer {
    private String REFRESH_TOKEN;
    private final Path REFRESH_TOKEN_FILE = FabricLoader.getInstance().getConfigDir().resolve("ytchat_refresh_token");
    private final Collection<String> SCOPES =
            List.of("https://www.googleapis.com/auth/youtube.readonly");
    private final String APPLICATION_NAME = "YTChat";
    private final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();

    private void sayInChat(Text t) {
        MinecraftClient.getInstance().inGameHud.getChatHud().addMessage(t);
    }

    private void sayInChat(String s) {
        sayInChat(Text.literal(s));
    }

    private void saveRefreshToken(String refreshToken) throws IOException {
        REFRESH_TOKEN = refreshToken;
        Files.writeString(REFRESH_TOKEN_FILE, REFRESH_TOKEN);
    }

    private String getNewToken(String refreshToken, String clientId, String clientSecret, final NetHttpTransport transport) throws IOException {
        TokenResponse tokenResponse = new GoogleRefreshTokenRequest(transport, JSON_FACTORY,
                refreshToken, clientId, clientSecret).setScopes(SCOPES).setGrantType("refresh_token").execute();
        return tokenResponse.getAccessToken();
    }

    private Credential getCredentials(final NetHttpTransport httpTransport, String refreshToken) throws IOException {
        GoogleCredential credential = new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(JSON_FACTORY)
                .setClientSecrets(CLIENT_ID, CLIENT_SECRET)
                .build();

        credential.setAccessToken(getNewToken(refreshToken, CLIENT_ID, CLIENT_SECRET, httpTransport));
        credential.setRefreshToken(refreshToken);

        return credential;
    }

    private Credential authorize(final NetHttpTransport httpTransport) throws IOException {
        GoogleClientSecrets clientSecrets = new GoogleClientSecrets();
        GoogleClientSecrets.Details det = new GoogleClientSecrets.Details();
        det.setClientId(CLIENT_ID);
        det.setClientSecret(CLIENT_SECRET);
        clientSecrets.setInstalled(det);
        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets, SCOPES)
                        .build();
        AuthorizationCodeInstalledApp app = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver());
        String redirectUri = app.getReceiver().getRedirectUri();
        String authUrl = app.getFlow().newAuthorizationUrl().setRedirectUri(redirectUri).toString();
        this.sayInChat(Text.literal("Click to authorize the mod: ")
                .append(Text.literal("§b" + authUrl).setStyle(Style.EMPTY.withClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, authUrl))))
                .append(" ")
                .append(Text.literal("§f§l[COPY]").setStyle(Style.EMPTY.withClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, authUrl)))));
        Credential credential = flow.createAndStoreCredential(flow.newTokenRequest(app.getReceiver().waitForCode()).setRedirectUri(redirectUri).execute(), "user");
        return credential;
    }

    public CompletableFuture<YouTube> getService() throws GeneralSecurityException, IOException {
        final NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        try {
            File tokenfile = new File(String.valueOf(REFRESH_TOKEN_FILE));
            if (tokenfile.exists())
                REFRESH_TOKEN = Files.readString(REFRESH_TOKEN_FILE);
            else tokenfile.createNewFile();
        } catch (Exception e) {
            LOGGER.error("Couldn't open token file", e);
            sayInChat("§eCouldn't open token file");
        }
        return CompletableFuture.supplyAsync(() -> {
            Credential credential;
            try {
                try {
                    credential = getCredentials(httpTransport, REFRESH_TOKEN);
                } catch (Exception e) {
                    credential = authorize(httpTransport);
                }
                saveRefreshToken(credential.getRefreshToken());
                return new YouTube.Builder(httpTransport, JSON_FACTORY, credential)
                        .setApplicationName(APPLICATION_NAME)
                        .build();
            } catch (Exception e) {
                if (e instanceof IOException) {
                    LOGGER.error(e.getMessage(), e);
                    System.exit(1);
                } else {
                    sayInChat("§4An error occured. Check logs for more information.");
                    LOGGER.error(e.getMessage(), e);
                }
                return null;
            }
        });
    }

    private boolean chatPaused = false;
    private final Logger LOGGER = LoggerFactory.getLogger("ytchat");
    private Timer t;
    private CompletableFuture f;

    private int startbridge(CommandContext<FabricClientCommandSource> ctx) throws GeneralSecurityException, IOException {
        if (f != null) f.cancel(true);
        f = getService().thenAccept(this::startbridge);
        ClientLifecycleEvents.CLIENT_STOPPING.register((client) -> f.cancel(true));
        return 0;
    }

    private void startbridge(YouTube youtubeService) {
        if (youtubeService == null) {
            sayInChat("§4Failed to authenticate");
            return;
        }
        sayInChat("§aAuthenticated successfully");
        String chatId;
        try {
            YouTube.LiveBroadcasts.List request = youtubeService.liveBroadcasts()
                    .list(List.of("snippet,id"));
            LiveBroadcastListResponse response = request.setBroadcastType("all")
                    .setMine(true)
                    .execute();
            chatId = response.getItems().get(0).getSnippet().getLiveChatId();
        } catch (Exception e) {
            sayInChat("§4Couldn't get stream ID");
            return;
        }
        Date lastMessage = new Date(System.currentTimeMillis());
        Queue<Text> messages = new LinkedList<>();
        if (t != null)
            t.cancel();
        t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Get messages
                try {
                    YouTube.LiveChatMessages.List request = youtubeService.liveChatMessages()
                            .list(chatId, List.of("authorDetails,snippet")).setMaxResults(250L);
                    LiveChatMessageListResponse response = request.execute();
                    response.getItems().forEach((LiveChatMessage m) -> {
                        Date date = Date.from(Instant.parse(m.getSnippet().getPublishedAt().toString()));
                        if (!m.getSnippet().getHasDisplayContent() || date.getTime() <= lastMessage.getTime())
                            return;
                        String author = m.getAuthorDetails().getDisplayName();
                        String contents = m.getSnippet().getDisplayMessage();
                        boolean isOwner = m.getAuthorDetails().getIsChatOwner();
                        boolean isMod = m.getAuthorDetails().getIsChatModerator();
                        boolean isSuperChat = m.getAuthorDetails().getIsChatSponsor();
                        boolean isVerified = m.getAuthorDetails().getIsVerified();
                        String channelUrl = m.getAuthorDetails().getChannelUrl();
                        lastMessage.setTime(date.getTime());
                        messages.add(Text.literal("§0[§4YT§0] ")
                                .append(Text.literal(String.format("§f%s%s%s",
                                        isOwner ? "§6" : isMod ? "§9" : isSuperChat ? "§b" : "",
                                        (isOwner || isMod) ? "§l" : "",
                                        author
                                )).setStyle(Style.EMPTY.withClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, channelUrl))))
                                .append(String.format("§r§f%s: %s",
                                        isVerified ? "✔" : isMod ? "★" : "",
                                        contents
                                )));
                    });
                    InGameHud hud = MinecraftClient.getInstance().inGameHud;
                    if (hud != null && !chatPaused) {
                        while (!messages.isEmpty()) {
                            hud.getChatHud().addMessage(messages.remove());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 2500, 1000);
    }

    @Override
    public void onInitialize() {
        ClientCommandRegistrationCallback.EVENT.register(this::registerCommands);
    }

    private void registerCommands(CommandDispatcher<FabricClientCommandSource> dispatcher, CommandRegistryAccess registryAccess) {
        dispatcher.register(ClientCommandManager.literal("startbridge").executes((ctx) -> {
            try {
                return startbridge(ctx);
            } catch (Exception e) {
                LOGGER.error("Error happened in startbridge", e);
                return 0;
            }
        }));

        dispatcher.register(ClientCommandManager.literal("pausechat").executes((ctx) -> {
            chatPaused = true;
            sayInChat("Chat paused");
            return 0;
        }));
        dispatcher.register(ClientCommandManager.literal("resumechat").executes((ctx) -> {
            chatPaused = false;
            sayInChat("Chat resumed");
            return 0;
        }));
    }
}
