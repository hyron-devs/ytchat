# Chat Bridge for YouTube

See your YouTube stream chat in Minecraft chat  
There are plenty of mods that integrate Twitch chat with YouTube chat. I have noticed a lack of ones that do this but with YouTube. I decided to make one.  
This mod is a bridge between YouTube and Minecraft chat. It will read messages from your YouTube live stream and show them in your Minecraft chat.

## Setup

### Mod Download

**Needs Fabric API**

Download the .jar file and put it in your mods folder

## Manual Setup

### Get credentials

1. Go to [Google API Console Credentials](https://console.cloud.google.com/apis/credentials)
2. Create project
3. Configure OAuth consent screen
    1. Enter a name
    2. Set type to External
    3. Create
    4. Fill out app information however you want
    5. Add `https://www.googleapis.com/auth/youtube.readonly` to scopes
    6. Add yourself as a test user
4. Go to Enabled APIs & Services
    1. Enable YouTube Data API
5. Go to credentials
    1. Create credentials
    2. OAuth client ID
    3. Desktop app
    4. Use the credentials in [the next section](#Compile)

### Compile

1. Put credentials in `src/main/java/xyz/hyron/ytchat/ClientSecrets.java`:

```java
package xyz.hyron.ytchat;

public class ClientSecrets {
    public static final String CLIENT_ID = "<Google OAuth 2.0 Client ID>";
    public static final String CLIENT_SECRET = "<Google OAuth 2.0 Client Secret>";
}
```

2. `gradle build`
3. Your jar is in `build/libs`

## Usage

Use the /startbridge command. Click on the link and choose the channel you stream on.

#### Commands

- **/startbridge** - Authenticate and start the bridge
- **/pausechat** - Pause chat bridge
- **/resumechat** - Resume chat bridge

## License

This project is licensed under GPL-3.0

#### [Website](https://hyron.xyz/ytchat/)

#### [GitLab](https://gitlab.com/hyron-devs/ytchat)

#### [CurseForge](https://www.curseforge.com/minecraft/mc-mods/youtube-chat-bridge)
